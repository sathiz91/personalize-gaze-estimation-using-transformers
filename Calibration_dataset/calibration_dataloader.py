import torch
from torch.utils.data import DataLoader
import albumentations as A
from dataset_loader import  get_dataloaders
from Calibration_dataset.calibration_query import CalibrationDataset


def get_calibration_dataloaders(gaze_model, path_to_data: str, batch_size: int):
    dataloaders, all_persons = get_dataloaders(path_to_data, batch_size)
    
    calibration_dataloaders = []
    for train_dataloader, valid_dataloader in dataloaders:
        calibration_train_dataset = CalibrationDataset(gaze_model, train_dataloader.dataset)
        calibration_valid_dataset = CalibrationDataset(gaze_model, valid_dataloader.dataset)
        
        
        calibration_train_dataloader = DataLoader(calibration_train_dataset, batch_size=batch_size)
        calibration_valid_dataloader = DataLoader(calibration_valid_dataset, batch_size=batch_size)
                
        calibration_dataloaders.append((calibration_train_dataloader, calibration_valid_dataloader))
    
    return calibration_dataloaders
1673