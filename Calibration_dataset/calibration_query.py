import torch
from torch.utils.data import Dataset, DataLoader

class CalibrationDataset(Dataset):
    def __init__(self, gaze_model, dataset):
        self.gaze_model = gaze_model
        self.dataset = dataset

    def __len__(self):
        return len(self.dataset)

    def __getitem__(self, idx):
        data = self.dataset[idx]

        face_image = data['full_face_image']
        face_image_flipped = data['full_face_flipped_image']
        left_eye_image = data['left_eye_image']
        right_eye_image = data['right_eye_image']
        head_rotation_matrix = data['head_rotation_matrix']
        gaze_target = data['gaze_normalized']
        subject_ids = data['subject_id']
        embeddings = None

        if len(face_image.shape) == 3:
            face_image = face_image.unsqueeze(0)
        if len(face_image_flipped.shape) == 3:
            face_image_flipped = face_image_flipped.unsqueeze(0)
        if len(left_eye_image.shape) == 3:
            left_eye_image = left_eye_image.unsqueeze(0)
        if len(right_eye_image.shape) == 3:
            right_eye_image = right_eye_image.unsqueeze(0)

        with torch.no_grad():
            embeddings = self.gaze_model(face_image, face_image_flipped, left_eye_image, right_eye_image, head_rotation_matrix, subject_ids)

        left_eye_image_flat = left_eye_image.contiguous().view(left_eye_image.size(0), -1)
        right_eye_image_flat = right_eye_image.contiguous().view(right_eye_image.size(0), -1)
        head_rotation_matrix_flat = head_rotation_matrix.contiguous().view(head_rotation_matrix.size(0), -1)
        gaze_target_flat = gaze_target.contiguous().view(1, -1)
        embeddings_flat = embeddings.contiguous().view(embeddings.size(0), -1)

        left_query = torch.cat((
            embeddings_flat,
            left_eye_image_flat,
            head_rotation_matrix_flat.view(1,-1),
            gaze_target_flat), dim=1)

        right_query = torch.cat((
            embeddings_flat,
            right_eye_image_flat,
            head_rotation_matrix_flat.view(1,-1),
            gaze_target_flat), dim=1)


        return {
            'left_query': left_query,
            'right_query': right_query,
            'embeddings': embeddings_flat
        }