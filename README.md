# Personalize Gaze Estimation using Transformers

## Overview
This repository contains the implementation of the Personal Transformer-based Gaze Estimation (PTGE) model as discussed in ["Personal Transformer-based Gaze Estimation"](http://jcse.kiise.org/files/V17N2-01.pdf). The goal of this project is to build and evaluate a gaze estimation model that minimizes computational requirements during calibration by leveraging transformer architectures.

# Installation
To set up this project locally, follow these steps:

1. **Clone the repository:**
   ```bash
   git clone https://gitlab.com/sathiz91/personalize-gaze-estimation-using-transformers.git

    ```
2. Install dependencies using the provided Anaconda environment:

```
conda env create -f environment.yml
```


3. Prepare the dataset:
Use the below link to download the GazeCapture dataset and place it in the ./dataset/ directory.
```
wget https://perceptualui.org/files/datasets/MPIIFaceGaze.zip
```

## Data Preprocessing 

This repository contains the `preprocessing_data.py` script, which is designed for preprocessing MPIIFaceGaze dataset. The script processes images from a dataset to normalize them based on head orientation and gaze direction, computes the gaze angles, and saves both the images and gaze data for model training.

## Features
- **Image Normalization**: Normalizes images of eyes and faces to a standard format, ensuring uniformity across the dataset.
- **Gaze Calculation**: Computes gaze vectors and head rotation matrices using facial landmarks and head orientation data.
- **Multi-threaded Execution**: Leverages Python's `concurrent.futures` for faster processing of large image datasets.
- **Data Storage**: Saves computed datas in HDF5 format `./data/data.h5`, also saves the normalized face images, flipped face image, right eye images, flipped left eye images to the respective folders.


# Gaze model

## Files
`dataset_loader.py`: Contains the MPIIFaceGaze dataset class and functions for loading the dataset using the leave-one-out strategy.
`train_gaze_model.py`: Script for training the gaze model using ResNet-50 for feature extraction and subject-specific embeddings.

## GazeModel Class
1. **ResNet-50 :** The model uses ResNet-50 pre-trained on ImageNet for feature extraction from face and eye images.
2. **Embeddings :** Subject-specific embeddings are used to capture person-specific differences.
3. **Gaze Extractor (MLP) :** A multi-layer perceptron that concatenates the extracted features, head rotation matrix, and subject-specific embeddings to predict the gaze direction.

## Forward Method
1. **Face and Eye Features:** Extracts features from face and eye images using ResNet-50.
2. **Subject Embeddings:** Retrieves subject-specific embeddings.
3. **Concatenation:** Concatenates face features, flipped face features, eye features, head rotation matrix, and subject embeddings.
4. **Gaze Prediction:** Passes the concatenated features through the gaze extractor to predict the gaze direction.

## Model Optimization 
1. **Huber Loss Function:** A smooth L1 loss function used to calculate the error between predicted and target gaze directions.
2. **Adam Optimizer Parameters:** Set the β1, β2, and ε values as specified in the paper: betas=(0.9, 0.999) and eps=1e-7.
3. **Learning Rate Scheduling:** 
    - Use an initial learning rate of 3 × 10^−4 for the first 40 epochs.
    - After 40 epochs, switch to a learning rate of 1 × 10^−4 with a cosine decay schedule (optim.lr_scheduler.CosineAnnealingLR).
4. **Embedding Layer Training Control:** 
    - For the first 40 epochs, disable gradient updates for the subject-specific embeddings.
    - After 40 epochs, enable gradient updates for the embeddings.
5. **L2 Regularization on Embeddings:** Apply L2 regularization with a weight decay of 0.01 to the embedding layer parameters.


# Training and Validation Loop
1. **Training Loop:** The model is trained using the training dataloader, and the optimizer updates the model parameters to minimize the loss.
2. **Validation Loop:** After each epoch, the model is evaluated on the validation set to track its performance and ensure it generalizes well to unseen data.
3.**Save Model:** Model Checkpoint: Saves the trained model to a file.


# Calibration Model

## File
- `calibration_query.py:` Contains the dataset class for calibration.
- `calibration_dataloader.py:` Contains functions for loading the calibration dataset.
- `train_calibration_model.py:` Script for training the calibration model using a Transformer-based architecture.

## CalibrationModel Class
1. **Initial MLPs:** Uses MLPs to process the left and right query inputs.
2. **Transformer Encoder:** Utilizes a Transformer encoder to process the combined features from left and right queries.
3. **Final MLP:** Uses an MLP to generate the final predicted calibration parameters.
## Forward Method
1. **Left and Right Query Features:** Processes the left and right queries using MLPs.
2. **Combined Features:** Combines the left and right query features and passes them through a Transformer encoder.
3. **Calibration Parameters:** Uses the final MLP to generate the calibration parameters.


# Usage
## Run the preprocessing script:
```
python preprocessing_data.py

```
## Train the gaze model:
```
python train_gaze_model.py

```
## Train the calibration model:
```
python train_calibration_model.py

```

# Future Work
   - Implement the final adaptation step that uses both the gaze model and calibration model to calibrate the model for a new user
   - Evaluate the performance of the gaze model on additional datasets.
   - Explore further optimization techniques and model architectures.