import itertools
from typing import List, Tuple

import albumentations as A
import h5py
import numpy as np
import skimage.io
import torch
from albumentations.pytorch import ToTensorV2
from torch.utils.data import DataLoader, Dataset

def filter_persons_by_idx(file_names: List[str], keep_person_idxs: List[int]) -> List[int]:
    idx_per_person = [[] for _ in range(15)]
    if keep_person_idxs is not None:
        keep_person_idxs = [f'p{person_idx:02d}/' for person_idx in set(keep_person_idxs)]
        for idx, file_name in enumerate(file_names):
            if any(keep_person_idx in file_name for keep_person_idx in keep_person_idxs):
                person_idx = int(file_name.split('/')[-3][1:])
                idx_per_person[person_idx].append(idx)
    else:
        for idx, file_name in enumerate(file_names):
            person_idx = int(file_name.split('/')[-3][1:])
            idx_per_person[person_idx].append(idx)

    return list(itertools.chain(*idx_per_person))

class MPIIFaceGaze(Dataset):
    def __init__(self, data_path: str, file_name: str, keep_person_idxs: List[int], transform=None, train: bool = False, force_flip: bool = False):
        if keep_person_idxs is not None:
            assert len(keep_person_idxs) > 0
            assert max(keep_person_idxs) <= 14  # last person id = 14
            assert min(keep_person_idxs) >= 0  # first person id = 0

        self.data_path = data_path
        self.hdf5_file_name = f'{data_path}/{file_name}'
        self.h5_file = None

        self.transform = transform
        self.train = train
        self.force_flip = force_flip

        with h5py.File(self.hdf5_file_name, 'r') as f:
            file_names = [file_name.decode('utf-8') for file_name in f['file_name_base']]

        by_person_idx = filter_persons_by_idx(file_names, keep_person_idxs)
        self.idx2ValidIdx = by_person_idx

    def __len__(self) -> int:
        return len(self.idx2ValidIdx) * 2 if self.train else len(self.idx2ValidIdx)

    def __del__(self):
        if self.h5_file is not None:
            self.h5_file.close()

    def open_hdf5(self):
        self.h5_file = h5py.File(self.hdf5_file_name, 'r')

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()

        if self.h5_file is None:
            self.open_hdf5()

        augmented_person = idx >= len(self.idx2ValidIdx)
        if augmented_person:
            idx -= len(self.idx2ValidIdx)  # fix idx

        idx = self.idx2ValidIdx[idx]

        file_name = self.h5_file['file_name_base'][idx].decode('utf-8')
        head_rotation_matrix = torch.tensor(self.h5_file['head_rotation_matrix'][idx], dtype=torch.float32)

        subject_id_str = self.h5_file['subject_id'][idx].decode('utf-8')
        subject_id = int(subject_id_str[1:])  # Extract the numerical part and convert to integer
        subject_id = torch.tensor(subject_id, dtype=torch.long)
        
        gaze_normalized = torch.tensor(self.h5_file['gaze_normalized'][idx], dtype=torch.float32)

        left_eye_image = skimage.io.imread(f"{self.data_path}/{file_name}-left_eye_flipped.png")
        right_eye_image = skimage.io.imread(f"{self.data_path}/{file_name}-right_eye.png")
        full_face_image = skimage.io.imread(f"{self.data_path}/{file_name}-full_face.png")
        full_face_flipped_image = skimage.io.imread(f"{self.data_path}/{file_name}-full_face_flipped.png")

        if augmented_person or self.force_flip:
            left_eye_image = np.flip(left_eye_image, axis=1)
            right_eye_image = np.flip(right_eye_image, axis=1)
            full_face_image = np.flip(full_face_image, axis=1)
            full_face_flipped_image = np.flip(full_face_flipped_image, axis=1)
            gaze_normalized[0] *= -1

        if self.transform:
            left_eye_image = self.transform(image=left_eye_image)["image"]
            right_eye_image = self.transform(image=right_eye_image)["image"]
            full_face_image = self.transform(image=full_face_image)["image"]
            full_face_flipped_image = self.transform(image=full_face_flipped_image)["image"]
        else:
            left_eye_image = torch.from_numpy(left_eye_image).float()
            right_eye_image = torch.from_numpy(right_eye_image).float()
            full_face_image = torch.from_numpy(full_face_image).float()
            full_face_flipped_image = torch.from_numpy(full_face_flipped_image).float()

        return {
            'file_name': file_name,
            'head_rotation_matrix': head_rotation_matrix,
            'subject_id': subject_id,
            'gaze_normalized': gaze_normalized,
            'left_eye_image': left_eye_image,
            'right_eye_image': right_eye_image,
            'full_face_image': full_face_image,
            'full_face_flipped_image': full_face_flipped_image
        }

def get_dataloaders(path_to_data: str, batch_size: int) -> Tuple[List[Tuple[DataLoader, DataLoader]], List[int]]:
    transform = {
        'train': A.Compose([
            A.ShiftScaleRotate(p=1.0, shift_limit=0.2, scale_limit=0.1, rotate_limit=10),
            A.Normalize(),
            ToTensorV2()
        ]),
        'valid': A.Compose([
            A.Normalize(),
            ToTensorV2()
        ])
    }

    all_persons = list(range(15))
    dataloaders = []
    # Leave one out strategy
    for validate_person in all_persons:
        train_persons = [p for p in all_persons if p != validate_person]
        print('Train on persons:', train_persons)
        print('Validate on person:', validate_person)

        dataset_train = MPIIFaceGaze(path_to_data, 'data.h5', keep_person_idxs=train_persons, transform=transform['train'], train=True)
        train_dataloader = DataLoader(dataset_train, batch_size=batch_size, shuffle=True, num_workers=4)

        dataset_valid = MPIIFaceGaze(path_to_data, 'data.h5', keep_person_idxs=[validate_person], transform=transform['valid'])
        valid_dataloader = DataLoader(dataset_valid, batch_size=batch_size, shuffle=False, num_workers=4)

        dataloaders.append((train_dataloader, valid_dataloader))

    return dataloaders, all_persons
