import torch
import torch.nn as nn
import torchvision.models as models

class GazeModel(nn.Module):
    def __init__(self, num_subjects, embedding_dim=6):  # Set embedding_dim to 6
        super(GazeModel, self).__init__()
        resnet_face = models.resnet50(pretrained=True)
        resnet_eye = models.resnet50(pretrained=True)

        self.feature_extractor_face = nn.Sequential(*list(resnet_face.children())[:-2])
        self.feature_extractor_eye = nn.Sequential(*list(resnet_eye.children())[:-2])

        self.fc_face = nn.Linear(resnet_face.fc.in_features, 512)
        self.fc_eye = nn.Linear(resnet_eye.fc.in_features, 256)

        self.embeddings = nn.Embedding(num_subjects, embedding_dim)

        self.gaze_extractor = nn.Sequential(
            nn.Linear(512 * 2 + 256 * 2 + 9 + embedding_dim, 128),
            nn.ReLU(),
            nn.Linear(128, 64),
            nn.ReLU(),
            nn.Linear(64, 3)
        )

        self.reference_vector = None

    def forward(self, face_image, face_image_flipped, left_eye_image, right_eye_image, head_rotation_matrix, subject_ids):
        face_features = self.feature_extractor_face(face_image)
        face_features = nn.AdaptiveAvgPool2d((1, 1))(face_features)
        face_features = face_features.view(face_features.size(0), -1)
        face_features = self.fc_face(face_features)

        face_features_flipped = self.feature_extractor_face(face_image_flipped)
        face_features_flipped = nn.AdaptiveAvgPool2d((1, 1))(face_features_flipped)
        face_features_flipped = face_features_flipped.view(face_features_flipped.size(0), -1)
        face_features_flipped = self.fc_face(face_features_flipped)

        left_eye_features = self.feature_extractor_eye(left_eye_image)
        left_eye_features = nn.AdaptiveAvgPool2d((1, 1))(left_eye_features)
        left_eye_features = left_eye_features.view(left_eye_features.size(0), -1)
        left_eye_features = self.fc_eye(left_eye_features)

        right_eye_features = self.feature_extractor_eye(right_eye_image)
        right_eye_features = nn.AdaptiveAvgPool2d((1, 1))(right_eye_features)
        right_eye_features = right_eye_features.view(right_eye_features.size(0), -1)
        right_eye_features = self.fc_eye(right_eye_features)

        preference_vector = self.embeddings(subject_ids)

        if self.reference_vector is not None:
            preference_vector += self.reference_vector

        features = torch.cat((face_features, face_features_flipped, left_eye_features, right_eye_features, head_rotation_matrix.view(head_rotation_matrix.size(0), -1), preference_vector), dim=1)
        
        gaze = self.gaze_extractor(features)
        return gaze

    def create_query(self, face_image, face_image_flipped, eye_image, head_rotation_matrix):
        face_features = self.feature_extractor_face(face_image)
        face_features = nn.AdaptiveAvgPool2d((1, 1))(face_features)
        face_features = face_features.view(face_features.size(0), -1)
        face_features = self.fc_face(face_features)

        face_features_flipped = self.feature_extractor_face(face_image_flipped)
        face_features_flipped = nn.AdaptiveAvgPool2d((1, 1))(face_features_flipped)
        face_features_flipped = face_features_flipped.view(face_features_flipped.size(0), -1)
        face_features_flipped = self.fc_face(face_features_flipped)

        eye_features = self.feature_extractor_eye(eye_image)
        eye_features = nn.AdaptiveAvgPool2d((1, 1))(eye_features)
        eye_features = eye_features.view(eye_features.size(0), -1)
        eye_features = self.fc_eye(eye_features)

        query = torch.cat((face_features, face_features_flipped, eye_features, head_rotation_matrix.view(head_rotation_matrix.size(0), -1)), dim=1)
        
        return query

    def set_reference_vector(self, reference_vector):
        self.reference_vector = reference_vector
