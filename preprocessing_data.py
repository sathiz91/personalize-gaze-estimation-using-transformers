import glob
import pathlib
from argparse import ArgumentParser
from collections import defaultdict
from typing import Tuple

import cv2
import h5py
import numpy as np
import pandas as pd
import scipy.io
import skimage.io
from tqdm import tqdm
from concurrent.futures import ThreadPoolExecutor, as_completed

def get_matrices(camera_matrix: np.ndarray, distance_norm: int, center_point: np.ndarray, focal_norm: int, head_rotation_matrix: np.ndarray, image_output_size: Tuple[int, int]) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
    distance = np.linalg.norm(center_point)
    z_scale = distance_norm / distance

    cam_norm = np.array([
        [focal_norm, 0, image_output_size[0] / 2],
        [0, focal_norm, image_output_size[1] / 2],
        [0, 0, 1.0],
    ])

    scaling_matrix = np.array([
        [1.0, 0.0, 0.0],
        [0.0, 1.0, 0.0],
        [0.0, 0.0, z_scale],
    ])

    forward = (center_point / distance).reshape(3)
    down = np.cross(forward, head_rotation_matrix[:, 0])
    down /= np.linalg.norm(down)
    right = np.cross(down, forward)
    right /= np.linalg.norm(right)

    rotation_matrix = np.asarray([right, down, forward])
    transformation_matrix = np.dot(np.dot(cam_norm, scaling_matrix), np.dot(rotation_matrix, np.linalg.inv(camera_matrix)))

    return rotation_matrix, scaling_matrix, transformation_matrix

def equalize_hist_rgb(rgb_img: np.ndarray) -> np.ndarray:
    ycrcb_img = cv2.cvtColor(rgb_img, cv2.COLOR_RGB2YCrCb)
    ycrcb_img[:, :, 0] = cv2.equalizeHist(ycrcb_img[:, :, 0])
    equalized_img = cv2.cvtColor(ycrcb_img, cv2.COLOR_YCrCb2RGB)
    return equalized_img

def normalize_single_image(image: np.ndarray, head_rotation, gaze_target: np.ndarray, center_point: np.ndarray, camera_matrix: np.ndarray, is_eye: bool = True) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
    focal_norm = 960
    distance_norm = 500 if is_eye else 1600
    image_output_size = (96, 96) if is_eye else (96, 96)

    if gaze_target is not None:
        gaze_target = gaze_target.reshape((3, 1))

    head_rotation_matrix, _ = cv2.Rodrigues(head_rotation)
    rotation_matrix, scaling_matrix, transformation_matrix = get_matrices(camera_matrix, distance_norm, center_point, focal_norm, head_rotation_matrix, image_output_size)

    img_warped = cv2.warpPerspective(image, transformation_matrix, image_output_size)
    img_warped = equalize_hist_rgb(img_warped)

    if gaze_target is not None:
        gaze_normalized = gaze_target - center_point
        gaze_normalized = np.dot(rotation_matrix, gaze_normalized)
        gaze_normalized = gaze_normalized / np.linalg.norm(gaze_normalized)
    else:
        gaze_normalized = np.zeros(3)

    return img_warped, gaze_normalized.reshape(3), rotation_matrix

def process_image(image_path: str, person: str, day: str, face_model: np.ndarray, camera_matrix: np.ndarray, annotations: pd.DataFrame, output_path: str, screen_width_pixel: int, screen_height_pixel: int) -> dict:
    data = {}
    annotation = annotations.loc['/'.join(image_path.split('/')[-2:])]

    img = skimage.io.imread(image_path)
    height, width, _ = img.shape

    head_rotation = annotation[14:17].to_numpy().reshape(-1).astype(float)
    head_translation = annotation[17:20].to_numpy().reshape(-1).astype(float)
    gaze_target_3d = annotation[23:26].to_numpy().reshape(-1).astype(float)

    head_rotation_matrix, _ = cv2.Rodrigues(head_rotation)
    face_landmarks = np.dot(head_rotation_matrix, face_model) + head_translation.reshape((3, 1))
    left_eye_center = 0.5 * (face_landmarks[:, 2] + face_landmarks[:, 3]).reshape((3, 1))
    right_eye_center = 0.5 * (face_landmarks[:, 0] + face_landmarks[:, 1]).reshape((3, 1))
    face_center = face_landmarks.mean(axis=1).reshape((3, 1))

    img_warped_left_eye, _, _ = normalize_single_image(img, head_rotation, None, left_eye_center, camera_matrix, is_eye=True)
    img_warped_right_eye, _, _ = normalize_single_image(img, head_rotation, None, right_eye_center, camera_matrix, is_eye=True)
    img_warped_face, gaze_normalized, rotation_matrix = normalize_single_image(img, head_rotation, gaze_target_3d, face_center, camera_matrix, is_eye=False)

    img_warped_left_eye_flipped = cv2.flip(img_warped_left_eye, 1)
    img_warped_face_flipped = cv2.flip(img_warped_face, 1)

    gaze_pitch = np.arcsin(-gaze_normalized[1])
    gaze_yaw = np.arctan2(-gaze_normalized[0], -gaze_normalized[2])

    base_file_name = f'{person}/{day}/'
    pathlib.Path(f"{output_path}/{base_file_name}").mkdir(parents=True, exist_ok=True)
    base_file_name += f'{image_path.split("/")[-1][:-4]}'

    
    skimage.io.imsave(f"{output_path}/{base_file_name}-right_eye.png", img_warped_right_eye.astype(np.uint8), check_contrast=False)
    skimage.io.imsave(f"{output_path}/{base_file_name}-left_eye_flipped.png", img_warped_left_eye_flipped.astype(np.uint8), check_contrast=False)
    skimage.io.imsave(f"{output_path}/{base_file_name}-full_face.png", img_warped_face.astype(np.uint8), check_contrast=False)
    skimage.io.imsave(f"{output_path}/{base_file_name}-full_face_flipped.png", img_warped_face_flipped.astype(np.uint8), check_contrast=False)

    data['file_name_base'] = base_file_name
    data['screen_size'] = [screen_width_pixel, screen_height_pixel]
    data['head_rotation_matrix'] = rotation_matrix.tolist()
    data['subject_id'] = person
    data['gaze_normalized'] = gaze_normalized.tolist()

    return data

def main(input_path: str, output_path: str):
    data = defaultdict(list)

    face_model = scipy.io.loadmat('./dataset/MPIIGaze/6_points_based_face_model.mat')['model']

    for person_path in tqdm(sorted(glob.glob(f'{input_path}/p*')), desc='person'):
        person = person_path.split('/')[-1]

        camera_matrix = scipy.io.loadmat(f'{person_path}/Calibration/Camera.mat')['cameraMatrix']
        screen_size = scipy.io.loadmat(f'{person_path}/Calibration/screenSize.mat')
        screen_width_pixel = screen_size["width_pixel"].item()
        screen_height_pixel = screen_size["height_pixel"].item()
        annotations = pd.read_csv(f'{person_path}/{person}.txt', sep=' ', header=None, index_col=0)

        with ThreadPoolExecutor() as executor:
            futures = []
            for day_path in sorted(glob.glob(f'{person_path}/day*')):
                day = day_path.split('/')[-1]
                for image_path in sorted(glob.glob(f'{day_path}/*.jpg')):
                    futures.append(executor.submit(process_image, image_path, person, day, face_model, camera_matrix, annotations, output_path, screen_width_pixel, screen_height_pixel))

            for future in tqdm(as_completed(futures), total=len(futures), desc='Processing images'):
                result = future.result()
                for key, value in result.items():
                    data[key].append(value)

    with h5py.File(f'{output_path}/data.h5', 'w') as file:
        for key, value in data.items():
            if key == 'file_name_base' or key == 'subject_id':
                file.create_dataset(key, data=value, compression='gzip', chunks=True)
            else:
                value = np.asarray(value)
                file.create_dataset(key, data=value, shape=value.shape, compression='gzip', chunks=True)

if __name__ == '__main__':
    input_path = './dataset/MPIIFaceGaze'
    output_path = './data'

    main(input_path, output_path)
