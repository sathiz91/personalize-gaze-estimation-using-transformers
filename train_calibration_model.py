import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import DataLoader
from torchvision import models
from torch.nn import TransformerEncoder, TransformerEncoderLayer
from dataset_loader import get_dataloaders
from Calibration_dataset.calibration_query import CalibrationDataset
from Calibration_dataset.calibration_dataloader import get_calibration_dataloaders

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

#gaze model using ResNet-50 for feature extraction
class GazeModel(nn.Module):
    def __init__(self, num_subjects, embedding_dim=128):  
        super(GazeModel, self).__init__()
        resnet_face = models.resnet50(pretrained=True)
        resnet_eye = models.resnet50(pretrained=True)

        
        self.feature_extractor_face = nn.Sequential(*list(resnet_face.children())[:-2])
        self.feature_extractor_eye = nn.Sequential(*list(resnet_eye.children())[:-2])

        self.fc_face = nn.Linear(resnet_face.fc.in_features, 512)
        self.fc_eye = nn.Linear(resnet_eye.fc.in_features, 256)


        self.embeddings = nn.Embedding(num_subjects, embedding_dim)

        # Gaze extractor (MLP)
        self.gaze_extractor = nn.Sequential(
            nn.Linear(512 * 2 + 256 * 2 + 9 + embedding_dim, 128),  # (512*2 for both face and flipped face) + (256*2 for eyes) + 9 (rotation matrix) + embeddings
            nn.ReLU(),
            nn.Linear(128, 64),
            nn.ReLU(),
            nn.Linear(64, 3) 
        )

    def forward(self, face_image, face_image_flipped, left_eye_image, right_eye_image, head_rotation_matrix, subject_ids):
        face_features = self.feature_extractor_face(face_image)
        face_features = nn.AdaptiveAvgPool2d((1, 1))(face_features)
        face_features = face_features.view(face_features.size(0), -1)
        face_features = self.fc_face(face_features)

        face_features_flipped = self.feature_extractor_face(face_image_flipped)
        face_features_flipped = nn.AdaptiveAvgPool2d((1, 1))(face_features_flipped)
        face_features_flipped = face_features_flipped.view(face_features_flipped.size(0), -1)
        face_features_flipped = self.fc_face(face_features_flipped)

        left_eye_features = self.feature_extractor_eye(left_eye_image)
        left_eye_features = nn.AdaptiveAvgPool2d((1, 1))(left_eye_features)
        left_eye_features = left_eye_features.view(left_eye_features.size(0), -1)
        left_eye_features = self.fc_eye(left_eye_features)

        right_eye_features = self.feature_extractor_eye(right_eye_image)
        right_eye_features = nn.AdaptiveAvgPool2d((1, 1))(right_eye_features)
        right_eye_features = right_eye_features.view(right_eye_features.size(0), -1)
        right_eye_features = self.fc_eye(right_eye_features)

        embeddings = self.embeddings(subject_ids)
        embeddings = embeddings.view(1, -1)
        head_rotation_matrix = head_rotation_matrix.view(1, -1)

        # Concatenate features
        features = torch.cat((face_features, face_features_flipped, left_eye_features, right_eye_features, head_rotation_matrix, embeddings), dim=1)

        gaze = self.gaze_extractor(features)
        return gaze

#calibration model
class CalibrationModel(nn.Module):
    def __init__(self,  total_input_size, embedding_dim=50, num_transformer_layers=6, num_heads=4, dim_feedforward=2048):
        super(CalibrationModel, self).__init__()

        self.total_input_size = total_input_size
        self.embedding_dim = embedding_dim

        if embedding_dim % num_heads != 0:
            raise ValueError(f'embedding_dim must be divisible by num_heads. '
                             f'Current values: embedding_dim={embedding_dim}, num_heads={num_heads}')

        # Initial MLPs for left and right queries
        self.left_mlp = nn.Sequential(
            nn.Linear(self.total_input_size, 256),
            nn.ReLU(),
            nn.Linear(256, self.embedding_dim)
        )
        self.right_mlp = nn.Sequential(
            nn.Linear(self.total_input_size, 256),
            nn.ReLU(),
            nn.Linear(256, self.embedding_dim)
        )

        # Transformer Encoder
        encoder_layers = TransformerEncoderLayer(self.embedding_dim, num_heads, dim_feedforward)
        self.transformer_encoder = TransformerEncoder(encoder_layers, num_transformer_layers)

        # Final MLP to get predicted calibration parameters
        self.final_mlp = nn.Sequential(
            nn.Linear(self.embedding_dim, 128),
            nn.ReLU(),
            nn.Linear(128, self.embedding_dim)
        )

    def forward(self, left_query, right_query):
        left_features = self.left_mlp(left_query)
        right_features = self.right_mlp(right_query)

        combined_features = torch.stack((left_features, right_features), dim=0)
        transformer_output = self.transformer_encoder(combined_features)

        calibration_params = self.final_mlp(transformer_output.mean(dim=0))  # Mean pooling
        return calibration_params

# Loss functions
def accuracy_loss(C_q, C_hat):
    return torch.mean((C_q - C_hat) ** 2)

def angular_difference(a, b):
    
    if a.dim() == 1 and b.dim() == 1:
        cos_sim = torch.sum(a * b) / (torch.norm(a) * torch.norm(b))
    else:
        cos_sim = torch.sum(a * b, dim=1) / (torch.norm(a, dim=1) * torch.norm(b, dim=1))
    cos_sim = torch.clamp(cos_sim, -1.0, 1.0)
    return torch.acos(cos_sim)

def consistency_loss(C_q1, C_qi):
    B = C_qi.size(0)
    loss = 0.0
    for i in range(1, B):
        loss += angular_difference(C_q1, C_qi[i])
    return loss / (B - 1)


def evaluate_model(model, dataloader):
    model.eval()
    total_loss = 0
    with torch.no_grad():
        for data in dataloader:
            left_query = data['left_query'].to(device)
            right_query = data['right_query'].to(device)
            embeddings = data['embeddings'].to(device)

            left_query = left_query.view(left_query.size(0), -1)
            right_query = right_query.view(right_query.size(0), -1)

            predicted_params = model(left_query, right_query)
            loss_accuracy = accuracy_loss(predicted_params, embeddings)
            loss_consistency = consistency_loss(predicted_params[0], predicted_params)
            loss = loss_accuracy + loss_consistency
            total_loss += loss.item()
    return total_loss / len(dataloader)


def train_calibration_model(calibration_model, gaze_model, calibration_dataloaders, num_epochs=50, lr=1e-4):
    optimizer = optim.Adam(calibration_model.parameters(), lr=lr)
    scheduler = optim.lr_scheduler.CosineAnnealingLR(optimizer, T_max=num_epochs)

    calibration_model.train()
    for epoch in range(num_epochs):
        for calibration_train_dataloader, calibration_valid_dataloader in calibration_dataloaders:
            for data in calibration_train_dataloader:
                left_query = data['left_query'].to(device)
                right_query = data['right_query'].to(device)
                embeddings = data['embeddings'].to(device)

                left_query = left_query.view(left_query.size(0), -1)  # Reshape to (batch_size, total_input_size)
                right_query = right_query.view(right_query.size(0), -1)  # Reshape to (batch_size, total_input_size)

                optimizer.zero_grad()
                predicted_params = calibration_model(left_query, right_query)
                loss_accuracy = accuracy_loss(predicted_params, embeddings)
                loss_consistency = consistency_loss(predicted_params[0], predicted_params)
                loss = loss_accuracy + loss_consistency
                loss.backward()
                optimizer.step()

                print(f'Epoch [{epoch+1}/{num_epochs}], Batch [{data["left_query"].shape[0]}], Loss: {loss.item():.4f}')
                # print(f'Predicted params: {predicted_params[:5]}')  #  5 predicted params for inspection
                # print(f'Embeddings: {embeddings[:5]}')  #  5 embeddings for inspection


        scheduler.step()

        val_loss = evaluate_model(calibration_model, calibration_valid_dataloader)
        print(f'Epoch [{epoch+1}/{num_epochs}], Validation Loss: {val_loss:.4f}')


    # Save the calibration model
    torch.save(calibration_model.state_dict(), 'calibration_model.pth')

# Paths and parameters
path_to_data = './data'
batch_size = 8
num_epochs = 50
embedding_dim = 50  # Dimension of the subject-specific embeddings

# Initialize and load the pre-trained gaze model
gaze_model = GazeModel(num_subjects=15, embedding_dim=embedding_dim).to(device)
gaze_model.load_state_dict(torch.load('gaze_model.pth'))

# Get calibration dataloaders
calibration_dataloaders = get_calibration_dataloaders(gaze_model, path_to_data, batch_size)
gaze_model_embedding_dim = 50

num_heads = 5
face_features_dim = 512
face_features_flipped_dim = 512
left_eye_features_dim = 27648  # Check the dimensions after the flattening operation
right_eye_features_dim = 27648  # Check the dimensions after the flattening operation
head_rotation_matrix_dim = 9  # Flattened head rotation matrix
gaze_model_embedding_dim = 50  # Embeddings 
gaze_target_dim = 3

total_input_size = gaze_model_embedding_dim + left_eye_features_dim + head_rotation_matrix_dim + gaze_target_dim

embedding_dim_for_transformer = 50  

calibration_model = CalibrationModel(total_input_size, embedding_dim=embedding_dim_for_transformer, num_heads=num_heads).to(device)
train_calibration_model(calibration_model, gaze_model, calibration_dataloaders, num_epochs, lr=1e-4)