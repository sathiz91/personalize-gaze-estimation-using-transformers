import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import DataLoader
import torchvision.models as models
from albumentations.pytorch import ToTensorV2
import albumentations as A
from dataset_loader import get_dataloaders


device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

# gaze model using ResNet-50 for feature extraction
class GazeModel(nn.Module):
    def __init__(self, num_subjects, embedding_dim=50):
        super(GazeModel, self).__init__()
        resnet_face = models.resnet50(pretrained=True)
        resnet_eye = models.resnet50(pretrained=True)
        
        # Modify the last fully connected layers for feature extraction
        self.feature_extractor_face = nn.Sequential(*list(resnet_face.children())[:-2])
        self.feature_extractor_eye = nn.Sequential(*list(resnet_eye.children())[:-2])
        
        self.fc_face = nn.Linear(resnet_face.fc.in_features, 512)
        self.fc_eye = nn.Linear(resnet_eye.fc.in_features, 256)
        
        # Subject-wise embeddings
        self.embeddings = nn.Embedding(num_subjects, embedding_dim)
        
        # Gaze extractor (MLP)
        self.gaze_extractor = nn.Sequential(
            nn.Linear(512 * 2 + 256 * 2 + 9 + embedding_dim, 128),  # (512*2 for both face and flipped face) + (256*2 for eyes) + 9 (rotation matrix) + embeddings
            nn.ReLU(),
            nn.Linear(128, 64),
            nn.ReLU(),
            nn.Linear(64, 3)
        )

    def forward(self, face_image, face_image_flipped, left_eye_image, right_eye_image, head_rotation_matrix, subject_ids):
        face_features = self.feature_extractor_face(face_image)
        face_features = nn.AdaptiveAvgPool2d((1, 1))(face_features)
        face_features = face_features.view(face_features.size(0), -1)
        face_features = self.fc_face(face_features)

        face_features_flipped = self.feature_extractor_face(face_image_flipped)
        face_features_flipped = nn.AdaptiveAvgPool2d((1, 1))(face_features_flipped)
        face_features_flipped = face_features_flipped.view(face_features_flipped.size(0), -1)
        face_features_flipped = self.fc_face(face_features_flipped)
        
        left_eye_features = self.feature_extractor_eye(left_eye_image)
        left_eye_features = nn.AdaptiveAvgPool2d((1, 1))(left_eye_features)
        left_eye_features = left_eye_features.view(left_eye_features.size(0), -1)
        left_eye_features = self.fc_eye(left_eye_features)
        
        right_eye_features = self.feature_extractor_eye(right_eye_image)
        right_eye_features = nn.AdaptiveAvgPool2d((1, 1))(right_eye_features)
        right_eye_features = right_eye_features.view(right_eye_features.size(0), -1)
        right_eye_features = self.fc_eye(right_eye_features)
        
        # Get subject-specific embeddings
        preference_vector = self.embeddings(subject_ids)
        
        # Concatenate features and additional inputs
        features = torch.cat((face_features, face_features_flipped, left_eye_features, right_eye_features, head_rotation_matrix.view(head_rotation_matrix.size(0), -1), preference_vector), dim=1)
        
        
        gaze = self.gaze_extractor(features)
        return gaze

# Huber loss (smooth L1 loss)
def huber_loss(pred, target, delta=1.5):
    abs_error = torch.abs(pred - target)
    quadratic = torch.minimum(abs_error, torch.tensor(delta).to(device))
    linear = abs_error - quadratic
    loss = 0.5 * quadratic**2 + delta * linear
    return loss.mean()

# parameters
path_to_data = './data'
batch_size = 16  
initial_lr = 3e-4
num_epochs = 50
num_subjects = 15  
embedding_dim = 50  # Dimension of the subject-specific embeddings

#dataloaders leave-one-out strategy
dataloaders, all_persons = get_dataloaders(path_to_data, batch_size)

# Initialize model, loss function, and optimizer
model = GazeModel(num_subjects, embedding_dim).to(device)
criterion = huber_loss  # Huber loss function

# Split parameters to apply L2 regularization only to the embeddings
other_params = [p for name, p in model.named_parameters() if 'embeddings' not in name]
embedding_params = [p for name, p in model.named_parameters() if 'embeddings' in name]

optimizer = optim.Adam([
    {'params': other_params},
    {'params': embedding_params, 'weight_decay': 0.01}
], lr=initial_lr, betas=(0.9, 0.999), eps=1e-7)

# Scheduler for learning rate decay after 40 epochs
scheduler = optim.lr_scheduler.CosineAnnealingLR(optimizer, T_max=num_epochs-40, eta_min=1e-4)


for epoch in range(num_epochs):
    print(f"Starting epoch {epoch+1}/{num_epochs}")
    model.train()
    if epoch < 40:
        for param in model.embeddings.parameters():
            param.requires_grad = False
    else:
        for param in model.embeddings.parameters():
            param.requires_grad = True

    for i, (train_dataloader, valid_dataloader) in enumerate(dataloaders):
        print(f"Training on dataloader {i+1}/{len(dataloaders)}")
        for batch_idx, data in enumerate(train_dataloader):
            face_image = data['full_face_image'].to(device)
            face_image_flipped = data['full_face_flipped_image'].to(device)
            left_eye_image = data['left_eye_image'].to(device)
            right_eye_image = data['right_eye_image'].to(device)
            head_rotation_matrix = data['head_rotation_matrix'].to(device)
            subject_ids = data['subject_id'].to(device)
            gaze_target = data['gaze_normalized'].to(device)

            optimizer.zero_grad()
            outputs = model(face_image, face_image_flipped, left_eye_image, right_eye_image, head_rotation_matrix, subject_ids)
            loss = criterion(outputs, gaze_target)
            loss.backward()
            optimizer.step()

            if batch_idx % 10 == 0:
                print(f'Epoch [{epoch+1}/{num_epochs}], Step [{batch_idx+1}/{len(train_dataloader)}], Loss: {loss.item():.4f}')

    # Scheduler step after 40 epochs
    if epoch >= 40:
        scheduler.step()

    
    model.eval()
    with torch.no_grad():
        valid_loss = 0
        for batch_idx, data in enumerate(valid_dataloader):
            face_image = data['full_face_image'].to(device)
            face_image_flipped = data['full_face_flipped_image'].to(device)
            left_eye_image = data['left_eye_image'].to(device)
            right_eye_image = data['right_eye_image'].to(device)
            head_rotation_matrix = data['head_rotation_matrix'].to(device)
            subject_ids = data['subject_id'].to(device)
            gaze_target = data['gaze_normalized'].to(device)

            outputs = model(face_image, face_image_flipped, left_eye_image, right_eye_image, head_rotation_matrix, subject_ids)
            loss = criterion(outputs, gaze_target)
            valid_loss += loss.item()

            if batch_idx % 10 == 0:
                print(f'Validation Step [{batch_idx+1}/{len(valid_dataloader)}], Loss: {loss.item():.4f}')

        print(f'Epoch [{epoch+1}/{num_epochs}], Validation Loss: {valid_loss / len(valid_dataloader):.4f}')


torch.save(model.state_dict(), 'gaze_model.pth')
